package domain;

import java.util.LinkedList;
import java.util.List;

public class Simulationsergebnis {

    private class Tupel{
        int bezug;
        int lieferung;

        public Tupel(int bezug, int lieferung){
            this.bezug = bezug;
            this.lieferung = lieferung;
        }
        @Override
        public String toString(){
            return "{Bezug: " + bezug + " Lieferung: " + lieferung + "}";
        }
    }

    List<Tupel> days = new LinkedList<>();

    public void addDay(int bezug, int lieferung) {
        days.add(new Tupel(bezug, lieferung));
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();

        sb.append("Simulationsergebnis: \n");

        int i = 0;

        for (Tupel day : days) {
            sb.append("Day " + i++ + ":" + day + "\n");
        }

        return sb.toString();
    }
}
