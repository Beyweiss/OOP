package domain;

import javax.lang.model.type.PrimitiveType;

public class Speicher {

	private final int maxKap;
	private int aktKap;
	private int bezug;
	private int lieferung;

	private int bezugCnt;
	private int lieferungCnt;

	public Speicher(int maxKap, int aktKap, int bezug, int lieferung) {
		this.maxKap = maxKap;
		this.aktKap = aktKap;
		this.bezug = bezug;
		this.lieferung = lieferung;

		this.bezugCnt = 0;
		this.lieferungCnt = 0;
	}

	public Speicher(int maxKap, int aktKap) {
		this.maxKap = maxKap;
		this.aktKap = aktKap;
		this.bezug = 0;
		this.lieferung = 0;

		this.bezugCnt = 0;
		this.lieferungCnt = 0;
	}

	/**
	 *
	 * @param diff die Aenderung der Kapazitaet
	 * @return die aktuelle Kapazitaet
	 */
	public int aenderKap(int diff) {
		if(diff < 0) {
			aktKap = minusKap(diff * -1);
		} else {
			aktKap = plusKap(diff);
		}
		return aktKap;
	}

	/**
	 *
	 * @param dazu wie viel kommt dazu
	 * @return aktueller stand
	 */
	private int plusKap(int dazu) {
		if(dazu + aktKap > maxKap) {
			lieferung += (aktKap + dazu) - maxKap;
			lieferungCnt ++;
			return maxKap;
		} else {
			return aktKap + dazu;
		}
	}

	/**
	 *
	 * @param weg wie viel wird gebraucht
	 * @return aktueller stand
	 */
	private int minusKap(int weg) {
		if(aktKap - weg < 0) {
			bezug -= aktKap - weg;
			bezugCnt ++;
			return 0;
		} else {
			return aktKap - weg;
		}
	}

	public void reset() {
	    bezug = 0;
	    lieferung = 0;
	    bezugCnt = 0;
	    lieferungCnt = 0;
    }

	public int getMaxKap() {
		return maxKap;
	}

	public int getAktKap() {
		return aktKap;
	}

	public int getBezug() {
		return bezug;
	}

	public int getLieferung() {
		return lieferung;
	}

	public int getBezugCnt() {
		return bezugCnt;
	}

	public int getLieferungCnt() {
		return lieferungCnt;
	}
}
