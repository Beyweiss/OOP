package domain;

/**
 * Created by fedors on 16/10/2017.
 */
public enum Wetterart {

	REGEN(0), TROCKEN(1), SONNE(2);

	private int numVal;

	Wetterart(int numVal) {
		this.numVal = numVal;
	}

	public int getNumVal() {
		return numVal;
	}
}
