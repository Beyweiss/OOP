package domain;

import java.util.Random;

/**
 * Created by fedors on 16/10/2017.
 */
public class Quelle {

    private static final double PROD_REGEN = 0.5;

    int size;
    Simulationsart simulationsart;
    Random random = new Random();

    public Quelle(int size, Simulationsart simulationsart) {
        this.size = size;
        this.simulationsart = simulationsart;
    }

    public int getProduction(Wetterart wetterart, boolean day) {
        switch(simulationsart) {
            case WASSER:
                return getProductionWater(wetterart);
            case STROM:
                return getProductionSun(wetterart, day);
        }
        return 0;
    }

    private int getProductionWater(Wetterart wetterart) {
        switch(wetterart) {
            case REGEN:
                return (int) (random.nextDouble() * size);
        }

        return 0;
    }

    private int getProductionSun(Wetterart wetterart, boolean day) {
        if(!day)
            return 0;

        double result = size * (0.1 + random.nextDouble() * 0.9);

        switch(wetterart) {
            case REGEN:
                return (int) (PROD_REGEN * result);
        }

        return (int) result;
    }
}
