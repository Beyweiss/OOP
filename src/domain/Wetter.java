package domain;

import java.util.Random;

/**
 * Created by michael on 16.10.17.
 */
public class Wetter {

    static Wetter theOne = null;

    Random rand = new Random();
    int raster;

    private Wetter(int raster){
        this.raster = raster;
    }

    public static Wetter getInstance(int raster){
        if (theOne == null){
            theOne = new Wetter(raster);
        }
        return theOne;
    }

    public Wetterart getWetterart(int time){
        int div = (int)(rand.nextDouble() * 10) + 1;

        if (time > raster/2){
            if (1/div == 1){
                return Wetterart.REGEN;
            }else {
                return Wetterart.TROCKEN;
            }
        }else{
            if (1/div == 1){
                return Wetterart.REGEN;
            }else {
                if (rand.nextBoolean()){
                    return Wetterart.TROCKEN;
                }else {
                    return Wetterart.SONNE;
                }
            }
        }
    }
}
