package domain;

import java.util.List;

/**
 * Created by fedors on 16/10/2017.
 */
public class Kontroller {

    private List<Verbraucher> verbraucher;
    private Quelle quelle;
    private Speicher speicher;
    private int raster;
    private Simulationsart simArt;
    private Wetter wetter;

    public Kontroller(int raster, Speicher speicher, Quelle quelle, List<Verbraucher> verbraucher, Simulationsart simArt){
        this.quelle = quelle;
        this.speicher = speicher;
        this.raster = raster;
        this.verbraucher = verbraucher;
        this.simArt = simArt;
        this.wetter = Wetter.getInstance(raster);
    }

    public Simulationsergebnis simulateDays(int days){

        Simulationsergebnis result = new Simulationsergebnis();

        for(int j = 0; j < days; j++) {

            System.out.println("Tag " + j);

            for (int i = 0; i < raster; i++){

                System.out.println("Raster: " + i + "/" + raster);

                Wetterart wetterart = wetter.getWetterart(i);

                System.out.println(wetterart);

                boolean day = i < raster / 2;

                int erzeugung = quelle.getProduction(wetterart, day);

                System.out.println("Erzeugt: " + erzeugung);

                int speicherstand = speicher.aenderKap(erzeugung);

                System.out.println("NeuerSpeicherstand: " + speicherstand);

                for (Verbraucher v : verbraucher) {
                    speicherstand = speicher.aenderKap(-v.verbrauche(speicherstand,wetterart, day));
                }

                System.out.println("NachVerbrauch: " + speicherstand + "\n");
            }
            result.addDay(speicher.getBezug(), speicher.getLieferung());
        }

        return result;
    }
}
