package domain;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by fedors on 16/10/2017.
 */
public class Test {

	private static List<Verbraucher> erzeugeVerbraucher(Simulationsart simulationsart) {
		LinkedList<Verbraucher> result = new LinkedList<>();
		result.add(new Verbraucher(false, simulationsart, 1000));
		result.add(new Verbraucher(true, simulationsart, 3500));
		result.add(new Verbraucher(false, simulationsart, 245));
		result.add(new Verbraucher(false, simulationsart, 1235));
		result.add(new Verbraucher(true, simulationsart, 9392));
		return result;
	}

	public static void main(String[] args) {
		Simulationsart simulationsart = Simulationsart.WASSER;
		Speicher speicher = new Speicher(130000, 20000);
		Quelle quelle = new Quelle(20000, simulationsart);

		Kontroller kontroller = new Kontroller(24, speicher, quelle, erzeugeVerbraucher(simulationsart), simulationsart);

		System.out.println(kontroller.simulateDays(2));
	}
}
