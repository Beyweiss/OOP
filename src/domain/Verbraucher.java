package domain;

import java.util.Random;

/**
 * Created by fedors on 16/10/2017.
 */
public class Verbraucher {
	private boolean optional;
	private Simulationsart simulationsart;

	private int verbrauch;

	private Random rand;

	private static final double trockenKoeffizient = 0.5;
	private static final double sonnenKoeffizient = 1;
	private static final int speicherSchwellwert = 35000;

	public Verbraucher(boolean optional, Simulationsart simulationsart, int verbrauch) {
		this.optional = optional;
		this.simulationsart = simulationsart;
		this.verbrauch = verbrauch;

		rand = new Random();
	}

	public int verbrauche(int speicherstand, Wetterart wetterart, boolean day) {
		if(simulationsart.equals(Simulationsart.WASSER)) {
			return verbraucheWasser(wetterart);
		} else if(simulationsart.equals(Simulationsart.STROM)) {
			return verbraucheStrom(speicherstand, day);
		}
		return 0;
	}

	private int verbraucheWasser(Wetterart wetterart) {
		if(wetterart.equals(Wetterart.REGEN)) {
			return 0;
		} else if(wetterart.equals(Wetterart.TROCKEN)) {
			if(rand.nextBoolean())
				return (int)(verbrauch * trockenKoeffizient);
		} else if(wetterart.equals(Wetterart.SONNE)) {
			if(rand.nextBoolean())
				return (int)(verbrauch * sonnenKoeffizient);
		}
		return 0;
	}

	private int verbraucheStrom(int speicherstand, boolean day) {
		if(optional == true) {
			return verbraucheStromOptional(speicherstand, day);
		} else {
			return verbraucheStromNormal();
		}
	}

	private int verbraucheStromNormal() {
		if(rand.nextBoolean()) {
			return verbrauch;
		} else {
			return 0;
		}
	}

	private int verbraucheStromOptional(int speicherstand, boolean day) {
		double wahrscheinlichkeit = 0.6 * rand.nextDouble();
		if(speicherstand > speicherSchwellwert) {
			wahrscheinlichkeit += 0.2;
		}
		if(day) {
			wahrscheinlichkeit += 0.2;
		}

		if(wahrscheinlichkeit > 0.5) {
			return verbrauch;
		}

		return 0;
	}
}
